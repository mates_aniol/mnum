#include <stdio.h>
#include <math.h>


double function(double x);

int main(void)
{   
    FILE *f = fopen("error.txt", "w");
    int i, min, max;
    double points, part, eval;
    min = -4;
    max = 10;
    points = 1002;
    part = (max - min)/points;

    printf("%e", part);
    for(i = 0; i <= points; i++)
    {
        eval = function(min + i*part);
        printf("Punt %20e:\t %.20e\t%.20e\t%.20e\t%.20e", min + i*part, eval, (float)eval, eval - (float)eval, (eval-(float)eval)/eval);
        fprintf(f, "%f\n", (eval - (float)eval));
        if((eval-(float)eval)/eval > 5.96e-8)
        {
            printf("!!!");
        }
        printf("\n");
    }
    return 0;
}

double function(double x)
{   
    return exp(x)*(1.1 + sin(2*M_PI*x));
}


