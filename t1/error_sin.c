#include <stdio.h>
#include <math.h>

int main(void)
{
    double k,fx, dfx;
    for(k = 1.0; k < 16; k++)
    {
        fx = cos(1.2);
        dfx = (sin(1.2 + 1/pow(10, k)) - sin(1.2))*pow(10,k);// + 1/pow(10, k);
        printf("exp=%+.0f\taprox=%+.15e\terr=%+.15e\n", k, dfx, dfx-fx);

    }
}
