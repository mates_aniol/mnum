#include <stdio.h>
#include <math.h>

int main(void)
{
    int n;
    double y,i;
    y = log(1.2); 

    printf("Per a quin terme vols calcular la integral? ");
    scanf("%d", &n);

    printf("Usant la recurrència y_n = 1/n -5*y_(n-1) amb y_0 = log(1.2):\n");
    for(i = 1.; i <= n; i++)
    {
        y = 1./(i) -5*y;
        printf("%15.8e", y);
        if(1./(6*(i+1)) >  y || y > 1./(5*(i+1)))
        {
            printf(" !!!\n");
        }
        else
        {
            printf("\n");
        }

    }
    

    printf("Usant la recurrència y_(n-1) = y_n/-5 + 1/(5*n) amb y_50 = 0:\n");
    y = 0.;
    for(i = 1.; i < n; i++)
    {
        y=y/-5 + 1./(5*(n-i));
        printf("%15.8e", y);
        if(1./(6*(n-i+1)) > y || y > 1./(5*(n-i+1)))
        {
            printf(" !!!\n");
        }
        else
        {
            printf("\n");
        }
    }return 0;
}

