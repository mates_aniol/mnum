#include <stdio.h>
#include <limits.h>


int factorial(int n);

int main()
{
    int i, ant;

    i = 1;
    ant = 0;
    
    /* Calculant límit superior */
    while(i >  ant)
    {
        ant = i;
        i += 1;
    }
    printf("El límit superior  és %d, i el límit superior teòric %d \n", ant, INT_MAX);
    

    /* Calculant límit inferior */
    i = -1;
    ant = 0;
    
    while(i < ant)
    {
        ant = i;
        i -= 1;
    }

    printf("El límit inferior trobat és %d i el límit inferior teòric és %d \n", ant, INT_MIN);

    i = INT_MAX;

    printf("%d + 1 = %d \n", i, i+1);
    printf("20! = %d \n", factorial(20));

    return 0;
}


int factorial(int n)
{
    int total = 1;
    for(int i = 2; i <= n; i++)
    {
        total *= i;
    }
    return total;
}
