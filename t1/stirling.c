#include <stdio.h>
#include <math.h>

double stirling(double n);
double stirling2(double n);

int main(void)
{
    int i, t[20][4];
    double stir, stir2;
    int factorial = 1; 
    printf("i\t\t\tAprox\t\t\tEabs\t\t\tErel\t\t\tAprox2\t\t\tEabs2\t\t\tErel2\n");
    for(i = 0; i < 10; i++)
    {
        factorial *= (i+1);
        stir = stirling(i+1);
        stir2 = stirling2(i+1);
        printf("%15d, %15d, %15.8e, %15.8e %15.8e, %15.8e, %15.8e, %15.8e\n", i+1, factorial, stir, factorial - stir, (factorial-stir)/factorial, stir2, factorial - stir2, (factorial-stir2)/factorial); 
    }
    return 0;
}   

double stirling(double n)
{
    return sqrt(2*M_PI*n)*pow((n/exp(1)),n);
}

double stirling2(double n)
{
    return stirling(n)*(1 + 1/(12*n));
}
