#include <stdio.h>
#include <math.h>

int main(void)
{ 
    float k, sum, reverse, square;
    double y_0, y_1;
    sum = 0;
    reverse = 0;
    square = 0;
    
    printf("Sumatori de 1/k del dret i del revés:\n");
    for(k = 1.0; k <= 100000; k++)
    {
        sum += 1/k;
        reverse += 1/(100001-k);
        square += 1/(pow(100001-k, 2)); 
        printf("%.0f\t %.12e\t %.12e\n",k, sum, reverse); 
    }

    printf("Sumatori de 1/(k^2) amb precisió màxima:\n");
    
    y_0 = 0.0;
    y_1 = 1.0;
    k = 2.0;
    while(y_1 + (1./k)/k > y_1 && (1./(k+1))/(k+1) < (1./k)/k) /* Parem quan s'acabi la precisió de la y_n o de la k */
    {
        y_1 += (1./k)/k;
        printf("%15.20e\t%15.8e\n", y_1, (1./k)/k);
        k++;
    }
    /*
    while(y_0 <= y_1)
    {
        y_0 = y_1;
        y_1 += (float)1/pow(k,2);
        k++;
    }*/
    printf("sum(1/(k^2)) = \%le\n", y_1); 
    return 0;

}
