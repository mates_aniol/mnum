/* zeros.c
 * Exemple de bon/mal condicionament d'un problema:
 * dependència dels zeros d'un polinomi respecte el coeficient de grau màxim
 *
 * Càlcul dels zeros del polinomi
 * p(x) = (x-1)(x-2)...(x-m) + c x**m ,
 * per al paràmetre  c  pròxim a  0
 * (quan c = 0, els zeros són coneguts: 1, 2, 3, ..., m)
 *
 * S'usa el mètode iteratiu de Newton per a buscar zeros de f(x) = 0:
 * x_0 aproximació inicial,
 * per a i >= 0:  x_{i+1} = x_{i} - f(x_{i}) / f'(x_{i})
 *
 * Resum del resultat:
 * Buscar un zero petit és un problema ben condicionat
 * Buscar un zero gran és un problema mal condicionat
 * */

#include <stdio.h>   /* per a scanf, printf */
#include <math.h>    /* per a pow, fabs */

int m;      /* variable global */

/* prototipus de les funcions */
double f(double x, double c);
double d(double x, double c);

/* funció principal */
int main(void)
{
    int k, i, iter;
    double c, x, xi, fi, eps;

    iter = 10;        /* nombre màxim d'iteracions permeses */
    eps = 1.e-10;     /* precisió desitjada */

    /* lectura de les dades: m, c */
    printf("m = ? (enter positiu), c = ? (real pròxima 0) \n");
    scanf("%d %le", &m, &c);
    if (m < 1 || c == 0) return 1;
    printf("m = %2d  c = %+le \n", m, c);

    /* cal calcular com varia, quan c != 0, cada zero x=k del cas c = 0 */
    for (k=1; k<=m; k++) {  
        x = k;
        xi = 1.;
        i = 0;
        while (i<iter && fabs(xi)>eps) {
            i = i+1;
            fi = f(x, c);
            xi = fi/d(x, c);
            x = x-xi;
            /* seguiment dels iterats; escriure-ho si alguna cosa falla
             *          printf("i=%2d x=%+.12le fi=%+.4le xi=%+.4le\n", i, x, fi, xi);
             *                   */
        }
        /* escriptura del resultat relatiu al zero x = k:
         *          o no s'ha convergit: missatge;
         *                   o sí que s'ha convergit: 
         *                               zero trobat, variació absoluta i variació relativa (respecte c) */
        if (fabs(xi)>eps) {
            printf("\n k = %2d, no hi hi hagut convergencia \n", k);
        } else {
            printf("\n k = %2d x_k = %+.12le   ", k, x);
            printf(" var abs: x_k = %+.4le var rel: dx_k/c = %+.4le \n", x-k, (x-k)/c);
        }
    }

    return 0;
}

/* funció que avalua p(x), per a cada valor de c */
double f(double x, double c) 
{
    int k;
    double y = 1.;

    for (k=1; k<=m; k++) y = y*(x-k);
    y = y+c*pow(x, m);

    return y;
}

/* funció que avalua p'(x), per a cada valor de c */
double d(double x, double c) 
{
    int k, j;
    double y, p;

    y = 0.;
    for (k=1; k<=m; k++) { 
        p = 1.;
        for (j=1; j<=m; j++) { 
            if (j!=k) p = p*(x-j);
        }
        y = y+p;
    }
    y = y+m*c*pow(x, m-1);

    return y;
}

