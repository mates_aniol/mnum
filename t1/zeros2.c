#include <stdio.h>
#include <math.h>


double f(double m, double c);
double df(double m, double c);


int main(void)
{
    int m;
    double c;
    printf("m = ? (enter positiu), c = ? (real pròxima 0) \n");
    scanf("%d %le", &m, &c);


}

/* funció que avalua p(x), per a cada valor de c */
double f(double x, double c) 
{
    int k;
    double y = 1.;

    for (k=1; k<=m; k++) y = y*(x-k);
    y = y+c*pow(x, m);

    return y;
}

/* funció que avalua p'(x), per a cada valor de c */
double df(double x, double c) 
{
    int k, j;
    double y, p;

    y = 0.;
    for (k=1; k<=m; k++) { 
        p = 1.;
        for (j=1; j<=m; j++) { 
            if (j!=k) p = p*(x-j);
        }
        y = y+p;
    }
    y = y+m*c*pow(x, m-1);

    return y;
}

