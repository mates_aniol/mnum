#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double det(int n, double **a, double tol);

int main(void)
{
    int *niub;
    int i, j, k;
    double determinant, c, d, sum, min, max, h;
    double **a, **a_cp;
    a = (double**)malloc(6*sizeof(double *));
    for(i = 0; i < 6; i++)
    {
        a[i] = (double*)malloc(6*sizeof(double));
    }
    a_cp = (double**)malloc(6*sizeof(double *));
    for(i = 0; i < 6; i++)
    {
        a_cp[i] = (double*)malloc(6*sizeof(double));
    }
    niub = (int*)malloc(8*sizeof(int));
    niub[0] = 2;
    niub[1] = 0;
    niub[2] = 0;
    niub[3] = 2;
    niub[4] = 6;
    niub[5] = 3;
    niub[6] = 4;
    niub[7] = 4;
    
    
    /*Omplim la matriu*/
    for(i = 0; i < 6; i++)
    {
        for(j = 0; j < 6; j++)
        {
            if(i == j)
            {
                a[i][j] = (double)(3*i+niub[i])/10.0;
            }
            else
            {
                a[i][j] = (double)(i+j)/50.0;
                
            }
            a_cp[i][j] = a[i][j];
        }
    }
    
    
    for(i = 0; i < 6; i++)
    {
        for(j = 0; j < 6; j++)
        {
            printf("%.3le ", a[i][j]);
        }
        printf("\n");
    }
    
    /* Calculem determinant per eliminacio gaussiana */
    
    determinant = det(6, a_cp, pow(10, -6));
    printf("\nEl determinant és: %.3le\n", determinant);
    
    
    for(i = 0; i < 6; i++)
    {
        sum = 0;
        for(j = 0; j < 6; j++)
        {
            if(i == j)
            {
                sum += fabs(a[i][j]);
            }
        }
        if(i == 0) /*Establim la primera */
        {
            min = a[i][i] - sum;
            max = a[i][i] + sum;
        }
        
        if(a[i][i] - sum < min)
        {
            min = a[i][i] - sum;
        }
        
        if(a[i][i] + sum > max)
        {
            max = a[i][i] + sum;
        }
    }
    c = min;
    d = max;
    
    printf("\nc: %.3le d: %.3le\n", c, d);
    
    h = 0.01;
    
    
    
    
    for(i = 0; i < 6; i++)
    {
        free(a[i]);
        free(a_cp[i]);
    }
    free(a_cp);
    free(a);
    free(niub);
    return 0;
    
    
}


double det(int n, double **a, double tol)
{
    int i, j, k, l, max, npiv;
    double m;
    double  *tmp;
    double det;

    /* Fem eliminacio gaussiana */
    npiv = 0;
    /* iterem sobre les columnes */
    for(k = 0; k < n-1; k++)
    {
        
        /* Fem pivotatge */
        max = k;
        for(l=k; l < n; l++)
        {
            if(fabs(a[l][k]) > fabs(a[max][k]))
            {
                max = l;
            }
        }
        if(max != k)
        {
            npiv++;
            tmp = a[k];
            a[k] = a[max];
            a[max] = tmp;
        }

        /* Reduim files */
        if(fabs(a[k][k]) < tol)
        {
            return 0;
        }
        for(i = k+1; i < n; i++)
        {
            m = a[i][k]/a[k][k];
            a[i][k] = 0; /* aquí podríem guardar m */
            for(j = k+1; j < n; j++)
            {
                a[i][j] -= m*a[k][j];
            }
        }
    }
    
    /* Calculem determinant (producte elements de la diagonal) */
    det = a[0][0];
    for(i = 1; i < n; i++)
    {
        det *= a[i][i];
    }
    
    if(npiv % 2 == 1)
    {
        det = -det;
    }
    
    return det;
    
    
    
}
