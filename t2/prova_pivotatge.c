#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

int main(void)
{
    int n, i, j;
    double **a;
    double c;
    printf("Introduïu la dimensió de la matriu (n): ");
    scanf("%d", &n);
    a = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        a[i] = (double*)malloc(n*sizeof(double));
    }
    printf("\nIntroduïu els elements de la matriu per files:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            scanf("%le", &c);
            a[i][j] = c;
        }
    }


    printf("\nMatriu:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            printf("%le ", a[i][j]);
        
        }
    }
    
    double *tmp;
    tmp = a[0];
    a[0] = a[1];
    a[1] = tmp;

    printf("\nMatriu després de canviar files 1 i 2:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            printf("%le ", a[i][j]);
        
        }
    }

    for(i = 0; i < n; i++)
    {
        free(a[i]);
    }
    free(a);
    return 0;
}
