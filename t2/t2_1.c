#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(void)
{
    int n, m, i, j;
    int **A, *y, *y1, *x;
    clock_t inici, final;
    printf("Introduïu la mida de la matriu a generar (m x n): ");
    scanf("%d%d", &m, &n);

    /* Fem malloc de les tres matrius que utilitzarem */
    A = malloc(m*sizeof(int *));
    for(i = 0; i < m; i++)
    {
        A[i] = malloc(n*sizeof(int));
    }
    x = malloc(n*sizeof(int));
    y = malloc(m*sizeof(int));
    y1 = malloc(m*sizeof(int));
    
    /* Inicialitzem les matrius i vectors amb valors aleatoris */
    printf("A:\n");
    for(i = 0; i < m; i++)
    {
        printf("\t");
        for(j = 0; j < n; j++)
        {
           A[i][j] = rand()%100;
           printf("%2d ", A[i][j]);
        }
        printf("\n");
    }
    
    printf("\nx:\n");
    for(i = 0; i < n; i++)
    {
        x[i] = rand()%100;
        printf("\t%d\n", x[i]);
    }

    printf("\ny:\n");
    for(i = 0; i < m; i++)
    {
        y[i] = rand()%100;
        printf("\t%d\n", y[i]);
        y1[i] = 0;
    }

    /* Fem l'assignació per files (o columnes ???) */
    inici = clock();
    for(i = 0; i < m; i++)
    {
        for(j = 0; j < n; j++)
        {
            y1[i] += A[i][j]*x[j];
        }
        y1[i] += y[i];
    }
    final = clock();
    
    /* Tornem a l'estat inicial */
    printf("\ny1 per files:\t\tTemps que ha trigat: %ld\n\n", final-inici);
    for(i = 0; i < m; i++)
    {
        printf("\t%d\n", y1[i]);
        y1[i] = 0;
    }

    /* Fem l'assignació al contrari que abans (ja no sé si són files o columnes) */
    inici = clock();
    for(j = 0; j < n; j++)
    {
        for(i = 0; i < m; i++)
        {
            y1[i] += A[i][j]*x[j]; 
        }
    }
    for(i = 0; i < m; i++)
    {
        y1[i] += y[i];
    }
    final = clock();
    
    printf("\ny1 per columnes:\tTemps que ha trigat: %ld\n\n", final-inici);
    for(i = 0; i < m; i++)
    {
        printf("\t%d\n", y1[i]);
        y1[i] = 0;
    }
    
    /* alliberem la memòria dinàmica */
    free(x);
    free(y);
    free(y1);

    for(i = 0; i < m; i++)
    {
        free(A[i]);
    }
    free(A);



    return 0;
}
