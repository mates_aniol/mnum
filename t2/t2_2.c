#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int gausspiv(int n, double **a, double *b, double tol);
int subst(int n, double **u, double *c, double tol);

int main(void)
{
    return 0;
}

int gausspiv(int n, double **a, double *b, double tol)
{
    int i, j, k;
    double m;
    double **a_ant, **a_act, **a_tmp;
    double *b_ant, *b_act, *b_tmp;
     **a_ant = malloc(n*sizeof(*double));
    for(i = 0; i < n; i++)
    {
        a_ant[i] = malloc(n*sizeof(double));
    }
    *b_ant = malloc(n*sizeof(double));

    **a_act = malloc(n*sizeof(*double));
    for(i = 0; i < n; i++)
    {
        a_act[i] = malloc(n*sizeof(double));
    }
    *b_act = malloc(n*sizeof(double));

    
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            a_ant[j][i] = a[j][i];
            a_act[j][i] = 0;
        }
        b_ant[i] = b[i];
        b_act[i] = 0;
    }
   

    for(k = 0; k < n-1; k++)
    {
	    /*aqui hauriem de pivotar  i treure aquest if*/
        for(i = k+1; i < n; i++)
        {

            if(fabs(a_ant[k][k] - 0) > tol)
            {
                return 1;
            }
	    /*Pots gardar m a a_(i,k) */
            m = a_ant[i][k]/a_ant[k][k];
            a_act[i][k] = 0;

	    /* no et calen 2 matrius, pots posar-ho a la mateixa */
            for(j = k+1; j < n; j++)
            {
                a_act[i][j] = a_ant[i][j] - m*a_ant[k][j];
            }
            b_act[i] = b_ant[i] - m*a_ant[k][j];
        }
        a_tmp = a_act;
        a_act = a_ant;
        a_ant = a_tmp;

        b_tmp = b_act;
        b_act = b_ant;
        b_ant = a_tmp;
    }
    

    for(i = 0; i < n; i++)
    {
        free(a_ant[i]);
        free(a_act[i]);
    }
    free(a_act);
    free(a_ant);
    free(a_tmp);
    free(b_act);
    free(b_ant);
    free(b_tmp);
    return 0;
}

/**

 per i de n-1 fins 0
	per j  de i+1 a n-1
		b_i = b_i - a_ij*b_j
	aqui comprovar si a_ii es proxim a 0 return
	b_i = b_i/a_ii
 *
 *
 *






 
