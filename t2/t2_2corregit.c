#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

int gausspiv(int n, double **a, double *b, double tol);
int subst(int n, double **u, double *c, double tol);
int eg(int n, double **a, double *b, double tol);

int main(void)
{
    int n, i, j;
    double **a;
    double *b;
    double c;
    printf("Introduïu la dimensió de la matriu (n): ");
    scanf("%d", &n);
    a = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        a[i] = (double*)malloc(n*sizeof(double));
    }
    b = (double*)malloc(n*sizeof(double));
    printf("\nIntroduïu els elements de la matriu per files:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            scanf("%le", &c);
            a[i][j] = c;
        }
    }

    printf("\nIntroduïu el terme independent:\n");
    for(i = 0; i < n; i++)
    {
        scanf("%le", &c);
        b[i] = c;
    }

    if(gausspiv(n, a, b, 0.05))
    {
        printf("\nNo s'ha pogut calcular");
        return 1;
    }   

    printf("\nMatriu després de la eliminació:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            printf("%le ", a[i][j]);
        
        }
        printf("%le\n", b[i]);
    }
    
    subst(n, a, b, 0.05);
    
    printf("\nSolucions del sistema: \n");
    
    for(i = 0; i < n; i++)
    {
        free(a[i]);
        printf("%le\n", b[i]);
    }
    free(a);
    free(b);
    return 0;
}

int gausspiv(int n, double **a, double *b, double tol)
{
    int i, j, k, l, max;
    double m, tmpd;
    double  *tmp, *tmp2;

    /* iterem sobre les columnes */
    for(k = 0; k < n-1; k++)
    {
        
        /* Fem pivotatge */
        max = k;
        for(l=k; l < n; l++)
        {
            if(fabs(a[l][k]) > fabs(a[max][k]))
            {
                max = l;
            }
        }
        if(max != k)
        {
            tmp = a[k];
            a[k] = a[max];
            a[max] = tmp;
            
            tmpd = b[k];
            b[k] = b[max];
            b[max] = tmpd;
        }

        /* Reduim files */
        if(fabs(a[k][k]) < tol)
        {
            return 1;
        }
        for(i = k+1; i < n; i++)
        {
            m = a[i][k]/a[k][k];
            a[i][k] = 0; /* aquí podríem guardar m */
            for(j = k+1; j < n; j++)
            {
                a[i][j] -= m*a[k][j];
            }
            b[i] = b[i] - m*b[k];
        } 
        
    }
    return 0;
}

int subst(int n, double **u, double *c, double tol)
{
    int i, j;
    double sum;
    /*c[n-1] = c[n-1]/u[n-1][n-1];*/
    for(i = n-1; i >= 0; i--)
    {
        sum = 0;
        for(j = i+1; j < n; j++)
        {
            sum += u[i][j]*c[j];
        }
        if(fabs(u[i][i]) < tol)
        {
            return 1;
        }
        c[i] = (c[i] - sum)/u[i][i]; 
    }   
    return 0;
}

int eg(int n, double **a, double *b, double tol)
{
    int k, i, j, l, max;
    double m;
    double *tmp;
    for(k = 0; k < n-1; k++) /* Iterem sobre columnes grans */
    {
        max = k;
        for(l=k; l < n; l++)
        {
            if(a[l][k] > a[max][k])
            {
                max = l;
            }
        }
        if(max != k)
        { 
            tmp = a[k];
            a[k] = a[max];
            a[max] = tmp;
        }       

        
        if(fabs(a[k][k]) < tol)
        {
            return 1;   
        }
        for(i = k+1; i < n; i++) /* Iterem sobre files */
        {
            m = a[i][k]/a[k][k];
            a[i][k] = 0;
            for(j = k+1; j < n; j++) /* Iterem sobre columnes */
            {
                a[i][j] = a[i][j] - m*a[k][j];
            }
            b[i] = b[i] - m*b[k];
        }
    }
    return 0;
}
/**

 per i de n-1 fins 0
	per j  de i+1 a n-1
		b_i = b_i - a_ij*b_j
	aqui comprovar si a_ii es proxim a 0 return
	b_i = b_i/a_ii
 *
 *
 */


