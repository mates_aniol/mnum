#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

int gaussjordanpiv(int n, double **a, double tol);

int main(void)
{
    int n, i, j;
    double **a;
    double *b;
    double c;
    printf("Introduïu la dimensió de la matriu (n): ");
    scanf("%d", &n);
    a = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        a[i] = (double*)calloc(2*n, sizeof(double));
    }
    
    printf("\nIntroduïu els elements de la matriu per files:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            scanf("%le", &c);
            a[i][j] = c;
        }
        a[i][n+i] = 1; /* La resta ja son 0's pel calloc */ 
    }

    if(gaussjordanpiv(n, a, 0.05))
    {
        printf("\nNo s'ha pogut calcular");
        return 1;
    }   

    /* Normalitzem la matriu reduida */
    for(i = 0; i < n; i++)
    {
        c = a[i][i];
        for(j = 0; j < 2*n; j++)
        {
            a[i][j] = a[i][j]/c;
        }
    }

    printf("\nMatriu després de la eliminació:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < 2*n; j++)
        {
            printf("%le ", a[i][j]);
        
        }
        printf("\n");
    }
    
    for(i = 0; i < n; i++)
    {
        free(a[i]);
    }
    free(a);
    return 0;
}

int gaussjordanpiv(int n, double **a, double tol)
{
    int i, j, k, l, max;
    double m, tmpd;
    double  *tmp, *tmp2;

    /* iterem sobre les columnes */
    for(k = 0; k < n; k++)
    {        
        /* Fem pivotatge */
        max = k;
        for(l=k; l < n; l++)
        {
            if(a[l][k] > a[max][k])
            {
                max = l;
            }
        }
        if(max != k)
        {
            tmp = a[k];
            a[k] = a[max];
            a[max] = tmp;   
        }
        /* Reduim files */
        if(fabs(a[k][k]) < tol)
        {
            return 1;
        }
        for(i = 0; i < n; i++)
        {
            if(i != k)
            {
                m = a[i][k]/a[k][k]; 
                for(j = 0; j < 2*n; j++)
                {
                    a[i][j] -= m*a[k][j];
                }
            }
        } 
        
    }
    return 0;
}



/**

 per i de n-1 fins 0
	per j  de i+1 a n-1
		b_i = b_i - a_ij*b_j
	aqui comprovar si a_ii es proxim a 0 return
	b_i = b_i/a_ii
 *
 *
 */


