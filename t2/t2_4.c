#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

#define TOL 0.05

int main(void)
{
    int n, i, j, k;
    double **a;
    double c;
    printf("Introduïu la dimensió de la matriu (n): ");
    scanf("%d", &n);
    a = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        a[i] = (double*)malloc(n*sizeof(double));
    }
   
    printf("\nIntroduïu els elements de la matriu per files:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            scanf("%le", &c);
            a[i][j] = c;
        }
    }

    for(k = 0; k < n-1; k++)
    {
        if(fabs(a[k][k]) < TOL)
        {
            printf("No es pot fer factorització LU");
            return 1;
        }

        for(i = k+1; i < n; i++)
        {
            a[i][k] = a[i][k]/a[k][k];
            for(j = k+1; j < n; j++)
            {
                a[i][j] -= a[i][k]*a[i][j];
            }
        }
    }

    printf("Matriu L:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            if(i == j)
            {
                printf("1 ");
            }
            else if(i < j)
            {
                printf("0 ");
            }
            else
            {
                printf("%le ", a[i][j]);
            }
        }
        printf("\n");
    }

    printf("\nMatriu U:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            if(i > j)
            {
                printf("0 ");
            }
            else
            {
                printf("%le ", a[i][j]);
            }
        }
        printf("\n");
    }
    
}

int alu(int n, double **a, double **l, double **u, double tol)
{
    int i, j, k;
    double sum;
    for(i = 0; i < n; i++)
    {
        /* Omplim la triangular superior */
        for(j = i; j < n; j++)
        {
            sum = 0;
            for(k = 0; k < i; k++)
            {
                sum += l[i][k]*u[k][j];
            }
            u[i][j] = a[i][j] - sum;
        }

        /* Omplim la triangular inferior */
        for(j = i; j < n; j++)
        {
            if(fabs(u[i][i]) < tol)
            {
                return 1;
            }

            if(i == j)
            {
                /*Si volem que ho guardi tot a la mateixa matriu (alu(n,a,a,a,tol)) cal comentar aquesta línia que sobreescriuria U.*/
                l[i][i] = 1;
            }
            else
            {
                sum = 0;
                for(k = 0; k < i; k++)
                {
                    sum += l[j][k]*u[k][i];
                }
                l[j][i] = (a[j][i] - sum)/u[i][i];
            }
        }

    }
    return 0;
}
