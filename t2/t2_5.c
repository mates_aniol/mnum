#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

int tridiag(int n, double *a, double *b, double *c, double *d);

int main(void)
{
    int n, i;
    double *a, *b, *c, *d;
    double e;
    printf("Introduiex la mida de la matriu trangular: ");
    scanf("%d", &n);
    a = (double *)malloc((n-1)*sizeof(double));
    b = (double *)malloc(n*sizeof(double));
    c = (double *)malloc((n-1)*sizeof(double));
    d = (double *)malloc(n*sizeof(double));

    printf("\nIntrodueix diagonal per sota la principal: ");
    for(i = 0; i < n-1; i++)
    {
        scanf("%le", &e);
        a[i] = e;
    }

    printf("\nIntrodueix diagonal principal: ");
    for(i = 0; i < n; i++)
    {
        scanf("%le", &e);
        b[i] = e;
    }

    printf("\nIntrodueix diagonal per sobre la principal: ");
    for(i = 0; i < n-1; i++)
    {
        scanf("%le", &e);
        c[i] = e;
    }


    printf("\nIntrodueix el terme independent: ");
    for(i = 0; i < n; i++)
    {
        scanf("%le", &e);
        d[i] = e;
    }

    tridiag(n, a, b, c, d);

    printf("\n\nSolucio de sistema tridiagonal:\n ");
    for(i = 0; i < n; i++)
    {
        printf("%le\n", d[i]);
    }
    
    return 0;

}

int tridiag(int n, double *a, double *b, double *c, double *d)
{
    int i;
    for(i = 0; i < n-1; i++)
    {
        a[i] = a[i]/b[i];
        b[i+1] -= a[i]*c[i];
        d[i+1] -= a[i]*d[i];
    }

    d[n-1] = d[n-1]/b[n-1];
    for(i = n-2; i >= 0; i--)
    {
        d[i] = (d[i] - c[i]*d[i+1])/b[i];
    }

}
