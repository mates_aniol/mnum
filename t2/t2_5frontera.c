/*Tenim 4 vestors, un d'ells és el terme independent, on guardarem 
 * la solució*/


#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define tol 1.e-14

double p(double x) { return 0; }
double q(double x) { return -1; }
double r(double x) { return 0; }

int resoltrid(int n, double *u, double *d, double *l, double *b){
    int i;
     
    for(i=0;i<n-1;i++){
        if (fabs(d[i])<tol) return -1;
        l[i]/=d[i];
        d[i+1]-=l[i]*u[i];
    }
         
    for(i=1;i<n;i++)
         b[i]-=l[i-1]*b[i-1];
 
    b[n-1]/=d[n-1];
    for(i=n-2;i>=0;i--){
        b[i]-=u[i]*b[i+1];
        b[i]/=d[i];
    }
     
    return 0;
}

int main(void){
    
    int i, n;
    double *sol, *l, *u, *d;
    double a, b, ya, yb, h, x;
    FILE *fout;
    
/*Demanem les dades prolema PVFL*/

    printf("Límits interval [a,b]?\n");
    scanf("%le %le", &a, &b);
    
    printf("Valor de y(a), y(b)?\n");
    scanf("%le %le", &ya, &yb);
    
    printf("Nombre de divisions de l'interval?\n"); /*Veure que això serà la dimensió de la matriu*/
    scanf("%d", &n);
    
/*Guardem memòria*/

    sol=(double*)malloc(n*sizeof(double));
    d=(double*)malloc(n*sizeof(double));
    l=(double*)malloc((n-1)*sizeof(double));    /*Veure que aquests tenen una component menys*/
    u=(double*)malloc((n-1)*sizeof(double));
    
    if(l==NULL || d==NULL || u==NULL || sol==NULL) return 1;
    
/*Fem els càlculs*/

    h = (b-a)/(n+1);
    
    x=a;
    for(i=0; i<n-1; i++){   /*Components del sistema tridiagonal*/
        x += h; 
        d[i]= 2+h*h*q(x);
        sol[i]= -h*h*r(x);
        l[i] = -1-p(x+h)*h/2;
        u[i] = -1+p(x)*h/2;
    }
    
    sol[0]+=(1+p(a+h)*h/2)*ya;  /*Afegim el que falta dels vectors de dim n*/
     
    d[n-1]=2+h*h*q(b-h);
    sol[n-1]=-h*h*r(b-h)+(1-p(b-h)*h/2)*yb;    
        
/*cridem resoltrid*/

    i=resoltrid(n,u,d,l,sol);
    if (i==-1) goto FREE;
     
    fout=fopen("2-4.out","w");
     
    fprintf(fout,"%le %le\n", a, ya);
    x=a;
    for(i=0;i<n;i++){
        x+=h;
        fprintf(fout,"%le %le\n", x, sol[i]);   
    }
    fprintf(fout,"%le %le\n", b, yb);
     
    fclose(fout);
     
    printf("\n");
     
    FREE:
     
    free(l);
    free(u);
    free(d);
    free(sol);
    
    return 0;
}
