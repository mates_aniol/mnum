#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

int llt(int n, double **c);
int utu(int n, double **c);

int main(void)
{
    int n, i, j;
    double **a;
    double *b;
    double c;
    printf("Introduïu la dimensió de la matriu (n): ");
    scanf("%d", &n);
    a = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        a[i] = (double*)malloc(n*sizeof(double));
    }
    b = (double*)malloc(n*sizeof(double));
    printf("\nIntroduïu els elements de la matriu per files:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            scanf("%le", &c);
            a[i][j] = c;
        }
    }

    llt(n, a);
    
    printf("Matriu L:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            if(i < j)
            {
                printf("0 ");
            }
            else
            {
                printf("%le ", a[i][j]);
            }
        }
        printf("\n");
    }
  
    for(i = 0; i < n; i++)
    {
        free(a[i]);
    }
    free(a);
    return 0;



}

int llt(int n, double **c)
{
    int i, j, k;
    double sum;

    for(i = 0; i < n; i++)
    {
        for(j = 0; j <= i; j++)
        {
            sum = 0;
            for(k = 0; k < j; k++)
            {
                sum += c[i][k]*c[j][k];
            }

            if(i == j)
            {
                c[i][j] = sqrt(c[i][i] - sum);
            }
            else
            {
                c[i][j] = (c[i][j] - sum)/c[j][j];
            }
        }
    }

}

int utu(int n, double **c)
{
    int i,j,k;
    double sum;

    for(i = 0; i < n; i++)
    {   
        sum = 0;
        for(k = 0; k < i; k++)
        {
            sum += pow(c[k][i], 2);
        }
        c[i][i] = sqrt(c[i][i] - sum);

        for(j = i+1; j < n; j++)
        {
            sum = 0;
            for(k = 0; k < i; k++)
            {
                sum += c[k][i]*c[k][j];
            }
            c[i][j] = (c[i][j] - sum)/c[i][i];
        }
    }
}
