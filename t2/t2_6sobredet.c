#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

int llt(int n, double **c);
int utu(int n, double **c);
int substendavant(int n, double **u, double *c, double tol);
int subst(int n, double **u, double *c, double tol);


int main(void)
{
    int n, m, i, j,k;
    double **a, **p;
    double *b, *bt, **lt;
    double c, sum;
    printf("Introduïu les dimensions de la matriu (n x m): ");
    scanf("%d", &n);
    scanf("%d", &m);
    a = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        a[i] = (double*)malloc(m*sizeof(double));
    }
    p = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        p[i] = (double*)malloc(n*sizeof(double));
    }
    lt = (double**)malloc(m*sizeof(double *));
    for(i = 0; i < m; i++)
    {
        a[i] = (double*)malloc(m*sizeof(double));
    }
    b = (double*)malloc(n*sizeof(double));
    bt = (double*)malloc(m*sizeof(double));
    printf("\nIntroduïu els elements de la matriu per files:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < m; j++)
        {
            scanf("%le", &c);
            a[i][j] = c;
        }
    }
    
    printf("\nIntroduïu el terme independent:\n");
    for(i = 0; i < n; i++)
    {
        scanf("%le", &c);
        b[i] = c;
    }
    
    /* fem el producte de a i la seva transposada */
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            sum = 0;
            for(k = 0; k < m; k++)
            {
                sum += a[i][k]*a[j][k];
            }
            p[i][j] = sum;
        }
    }
    
    /* Producte de la transposada i el terme independent */
    for(i = 0; i < m; i++)
    {
        sum = 0;
        for(j = 0; j < n; j++)
        {
            sum += a[j][i]*b[j];
        }
        bt[i] = sum;
    }
    
    /* Fem Cholesky del producte de A i A transposada*/
    llt(m, p);
    
    /* Creem la matriu superior de Cholesky */
    for(i = 0; i < m; i++)
    {
        for(j = i; j < m; j++)
        {
            lt[i][j] = p[j][i];
        }
    }
    
    substendavant(m, p, bt, 0.05);
    subst(m, lt, bt, 0.05);
    
    for(m = 0; i < n; i++)
    {
        printf("%le\n", lt[i]);
    }
    return 0;
    
    for(i = 0; i < n; i++)
    {
        free(a[i]);
    }
    free(a);
    return 0;



}

int llt(int n, double **c)
{
    int i, j, k;
    double sum;

    for(i = 0; i < n; i++)
    {
        for(j = 0; j <= i; j++)
        {
            sum = 0;
            for(k = 0; k < j; k++)
            {
                sum += c[i][k]*c[j][k];
            }

            if(i == j)
            {
                c[i][j] = sqrt(c[i][i] - sum);
            }
            else
            {
                c[i][j] = (c[i][j] - sum)/c[j][j];
            }
        }
    }

}

int utu(int n, double **c)
{
    int i,j,k;
    double sum;

    for(i = 0; i < n; i++)
    {   
        sum = 0;
        for(k = 0; k < i; k++)
        {
            sum += pow(c[k][i], 2);
        }
        c[i][i] = sqrt(c[i][i] - sum);

        for(j = i+1; j < n; j++)
        {
            sum = 0;
            for(k = 0; k < i; k++)
            {
                sum += c[k][i]*c[k][j];
            }
            c[i][j] = (c[i][j] - sum)/c[i][i];
        }
    }
}

int subst(int n, double **u, double *c, double tol)
{
    int i, j;
    double sum;
    for(i = n-1; i >= 0; i--)
    {
        sum = 0;
        for(j = i+1; j < n; j++)
        {
            sum += u[i][j]*c[j];
        }
        if(fabs(u[i][i]) < tol)
        {
            return 1;
        }
        c[i] = (c[i] - sum)/u[i][i]; 
    }   
    return 0;
}

int substendavant(int n, double **u, double *c, double tol)
{
    int i, j;
    double sum;
    for(i = 0; i < n; i++)
    {
        sum = 0;
        for(j = 0; j < i; j++)
        {
            sum += u[i][j]*c[j];
        }
        if(fabs(u[i][i]) < tol)
        {
            return 1;
        }
        c[i] = (c[i] - sum)/u[i][i]; 
    }   
    return 0;
}