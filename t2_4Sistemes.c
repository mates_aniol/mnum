#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <time.h>

#define TOL 0.05

int substendavant(int n, double **u, double *c, double tol);
int subst(int n, double **u, double *c, double tol);


int main(void)
{
    int n, i, j, k;
    double **a, **l, **u;
    double *x, *y;
    double c;
    printf("Introduïu la dimensió de la matriu (n): ");
    scanf("%d", &n);
    a = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        a[i] = (double*)malloc(n*sizeof(double));
    }
    l = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        l[i] = (double*)malloc(n*sizeof(double));
    }
    u = (double**)malloc(n*sizeof(double *));
    for(i = 0; i < n; i++)
    {
        u[i] = (double*)malloc(n*sizeof(double));
    }
    x = (double*)malloc(n*sizeof(double));
    y = (double*)malloc(n*sizeof(double));
    printf("\nIntroduïu els elements de la matriu per files:\n");
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            scanf("%le", &c);
            a[i][j] = c;
        }
    }

    printf("\nIntroduïu el terme independent:\n");
    for(i = 0; i < n; i++)
    {
        scanf("%le", &c);
        x[i] = c;
    }

    alu(n, a, l, u, TOL);
    
    substendavant(n, l, x, 0.05);
    subst(n, u, x, 0.05);
    
    for(i = 0; i < n; i++)
    {
        printf("%le\n", x[i]);
    }
    return 0;
}

int alu(int n, double **a, double **l, double **u, double tol)
{
    int i, j, k;
    double sum;
    for(i = 0; i < n; i++)
    {
        for(j = i; j < n; j++)
        {
            sum = 0;
            for(k = 0; k < i; k++)
            {
                sum += l[i][k]*u[k][j];
            }
            u[i][j] = a[i][j] - sum;
        }

        for(j = i; j < n; j++)
        {
            if(fabs(u[i][i]) < tol)
            {
                return 1;
            }

            if(i == j)
            {
                l[i][i] = 1;
            }
            else
            {
                sum = 0;
                for(k = 0; k < i; k++)
                {
                    sum += l[j][k]*u[k][i];
                }
                l[j][i] = (a[j][i] - sum)/u[i][i];
            }
        }

    }
    return 0;
}

int subst(int n, double **u, double *c, double tol)
{
    int i, j;
    double sum;
    for(i = n-1; i >= 0; i--)
    {
        sum = 0;
        for(j = i+1; j < n; j++)
        {
            sum += u[i][j]*c[j];
        }
        if(fabs(u[i][i]) < tol)
        {
            return 1;
        }
        c[i] = (c[i] - sum)/u[i][i]; 
    }   
    return 0;
}

int substendavant(int n, double **u, double *c, double tol)
{
    int i, j;
    double sum;
    for(i = 0; i < n; i++)
    {
        sum = 0;
        for(j = 0; j < i; j++)
        {
            sum += u[i][j]*c[j];
        }
        if(fabs(u[i][i]) < tol)
        {
            return 1;
        }
        c[i] = (c[i] - sum)/u[i][i]; 
    }   
    return 0;
}