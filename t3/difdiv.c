#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void difdiv(int n, double *x, double *f);
double horner(int n, double *x, double *f, double z);
double func(double x);

int main(void)
{
        int n, i, j;
        double k, c, d;
        double sup, inf;
        FILE *f = fopen("dat.dat", "w");

        printf("#Introduiu el grau del polinomi interpolador:");
        scanf("%d", &n);
        

        double * x = (double *)malloc((n+1)*sizeof(double));
        double * y = (double *)malloc((n+1)*sizeof(double));
        double * interp = (double *)malloc((n+1)*sizeof(double));

        printf("#En quin interval voleu interpolar?");
        scanf("%le", &inf);
        scanf("%le", &sup);


        for(i = 0; i < n+1; i++)
        {
            //Punts equidistants
            x[i] = inf + i*((double)sup-inf)/n;
            //Calculem els punts en la funcio pels cuals interpolarem
            y[i] = func(x[i]);
            printf( "%le %le\n", x[i], y[i]);
        }
        
        difdiv(n+1, x, y);
        printf("\n\n");
       
        for(i = 0; i< n+1; i++)
        {
            fprintf(f, "%le %le %le\n", x[i], func(x[i]), horner(n, x, y, x[i]));            
        }
    return 0;

}

void difdiv(int n, double* x, double* f) {
	int i, j;
	for(i = 1; i < n; i++) {
		for(j = n-1; j > i-1; j--) {
			f[j] = (f[j] - f[j-1]) / (x[j] - x[j-i]);
		}
	}
}
/*void difdiv(int n, double *x, double *f)
{
	int i, j;
	for(j = 0; j <= n; j++)
	{
		for(i = n; i >= j; i--)
		{
            f[i] = (f[i] - f[i-1])/(x[i] - x[i-j]);
		}
    }
}*/

double horner(int n, double *x, double *f, double z)
{
    int i;
    double p = 0.0;
	for(i = n; i >= 0; i--) /* i = n o i = n-1??? */
    {
        p = f[i] + (z-x[i])*p;
        printf("%le %le %le %le\n", f[i], z, x[i], p);
    }
    return p;
}

double func(double x)
{
    return 1.0/(1+x*x);
}
