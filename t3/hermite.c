#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void difdiv2(int n, double *x, double *f);
double horner(int n, double *x, double *f, double z);
double func(double x);

int main(void)
{
        int n, i, j;
        double k, c, d;
        double sup, inf;
        FILE *f = fopen("dat.dat", "w");

        printf("#Introduiu el grau del polinomi interpolador:");
        scanf("%d", &n);
        

        double * x = (double *)malloc((n+1)*sizeof(double));
        double * f = (double *)malloc((n+1)*sizeof(double));
        double * f' = (double *)malloc((n+1)*sizeof(double));
        double * q = (double **)malloc((2*n+1)*sizeof(double *));
        for(i = 0; i < 2*n+1; i++)
        {
            q[i] = (double*)malloc((2*n+1)*sizeof(double));
        }

        printf("#En quin interval voleu interpolar?");
        scanf("%le", &inf);
        scanf("%le", &sup);

        printf("Introduiu els punts (x_i, f_i, f'_i') coneguts en ordre creixent de les abscises:");
        for(i = 0; i < n; i++)
        {
                scanf("%le", &x[i]);
                scanf("%le", &f[i]);
                scanf("%le", &f[i])
        }

        
        difdivi2(n, x, f, f', q);
        printf("\n\n");
       
        for(k = inf; k <= sup; k+= ((double)sup-inf)/100.0)
        {
            fprintf(f, "%le %le\n", k, horner(n, x, q, k));            
        }
    return 0;

}

void difdiv(int n, double* x, double* f, double* f', double* q) 
{
    /* Et passo n, que és el grau del polinomi, així que hi ha n+1 punts */
    /* Falta declarar les coses */
    double * z = (double *)malloc((2*n+1)*sizeof(double));
	int i, j;
    for(i = 0; i <= n; i++)
    {
        z[2*i] = x[i];
        z[2*i+1] = x[i];
        q[2*i][0] = f[i];
        q[2*i+1][0] = f[i];
        q[2*i+1][1] = f[i];

        if(i != 0)
        {
            q[2*i][1] = (q[2*i][0] - q[2*1-1][0])/(z[2*i]-z[2*i-1]);
        }
    }

    for(i = 2; i <= 2*n+1; i++)
    {
        for(j = 2; j <= i; j++ )
        {
            q[i][j] = (q[i][j-1]-q[i-1][j-1])/(z[i]-z[i-j]);
        }
    }

}
/*void difdiv(int n, double *x, double *f)
{
	int i, j;
	for(j = 0; j <= n; j++)
	{
		for(i = n; i >= j; i--)
		{
            f[i] = (f[i] - f[i-1])/(x[i] - x[i-j]);
		}
    }
}*/

double horner(int n, double *x, double *f, double z)
{
    int i;
    double p = 0.0;
	for(i = n; i >= 0; i--) /* i = n o i = n-1??? */
    {
        p = f[i] + (z-x[i])*p;
        printf("%le %le %le %le\n", f[i], z, x[i], p);
    }
    return p;
}

double func(double x)
{
    return 1.0/(1+x*x);
}
