#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void difdiv2(int n, double *x, double *f, double *g);
void horner_general(int n, double *x, double *f, double z, double &p, double &d);
double func(double x);

int main(void)
{
        int n, i, j;
        double k, c, d;
        double y, dx;
        double sup, inf;
        FILE *file = fopen("dat.dat", "w");

        printf("#Introduiu el grau del polinomi interpolador:");
        scanf("%d", &n);
        

        double * x = (double *)malloc(2*(n+1)*sizeof(double));
        double * f = (double *)malloc(2*(n+1)*sizeof(double));
        double * g = (double *)malloc(2*(n+1)*sizeof(double));

        printf("#En quin interval voleu interpolar?");
        scanf("%le", &inf);
        scanf("%le", &sup);

        printf("Introduiu els punts (x_i, f_i, f'_i') coneguts en ordre creixent de les abscises:");
        for(i = 0; i <= n; i++)
        {
                scanf("%le", &x[2*i]);
                x[2*i+1] = x[2*i];
                scanf("%le", &f[2*i]);
                f[2*i+1] = f[2*i];
                scanf("%le", &g[i]);
        }
        /*printf("%le", f[2*n]);
        for(i = 0; i <= 2*n+1; i++)
        {
            printf("%le ", x[i]);
        }
        printf("\n");

         for(i = 0; i <= 2*n+1; i++)
        {
            printf("%le ", f[i]);
        }
        printf("\n");

        for(i = 0; i <= n; i++)
        {
            printf("%le ",g[i]);
        }
        printf("\n");

        difdiv2(2*n+2, x, f, g);
        for(i = 0; i < 2*n+2; i++)
        {
            printf("%le\n",f[i]);
        }
        printf("\n\n");*/
     
        difdiv2(2*n+2, x, f,g);
        for(k = inf; k <= sup; k+= ((double)sup-inf)/100.0)
        {
            horner_general(2*n+2, x, f, k, y, dx);
            fprintf(file, "%le %le %le\n", k, y, dx);            
        }
    return 0;

}

void difdiv2(int n, double* x, double* f, double* g) 
{
    int i, j;
    /* La  n d'aquí serà 2n+2 de fora, i tirem la sortida a f */
    /*for(i = 1; i < n; i++)*/
    for(i = n-1; i > 0; i--)
    {
        if(i%2 == 1)
        {
            f[i] = g[(i-1)/2];
        }
        else
        {
            /*f[i] = (f[i/2]-f[(i/2)-1])/(x[i/2]-x[(i/2)-1]);*/
            f[i] = (f[i]-f[i-1])/(x[i/1]-x[i-1]);
        }
    }
   

    /*i és el 'contador del pas'*/
   for(i = 2; i < n; i++)
    {
        /*en cada pas calcularem un valor menys*/º
        for(j = n-1; j > i-1; j--)
        {
            /*printf("j: %d\n, f_j: %le, f_j-1: %le, x_j: %le, x_j-i: %le", j, f[j], f[j-1], x[j], x[j-i]);*/
            /*f[j] = (f[j]-f[j-1])/(x[j]-x[j-i]);*/
            f[j] = (f[j]-f[j-1])/(x[j]-x[j-i]);
        }
    }

}

//n = nombre de punts duplicats, x les abscises, f coeficients de p(x) (un cop fet hermite), z el punt on volem evaluar, p resultat d'evaluar p (p(k)), d resultat d'evaluar la redivada (p'(k)).
void horner_general(int n, double *x, double *f, double z, double &p, double &d)
{
    int i;
    p = 0.0;
    d = 0.0;
	for(i = n; i >= 0; i--) /* i = n o i = n-1??? */
    {
        d = d*(z-x[i]) + p;
        p = f[i] + (z-x[i])*p;
        printf("%le %le %le %le %le\n", f[i], z, x[i], p, d);
    }
}

double func(double x)
{
    return 1.0/(1+x*x);
}
