#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double Li(int i, int n, double *x, double X);
double p(int n, double *x, double *y, double X);
double p_li(int n, double *y, double *li);

int main(void)
{
	/* ALERTA: MIRA SI FAS SERVIR N O N+1 !!! */
        int n, i, j;
        double k, c, d;

        FILE *f = fopen("data.txt", "w");

        printf("Introduiu el nombre de punts condeguts:");
        scanf("%d", &n);


        double * x = (double *)malloc(n*sizeof(double));
        double * y = (double *)malloc(n*sizeof(double));
        /*double * li = (double *)malloc(n*sizeof(double)); */

        /* Omplim la taula */
        printf("Introduiu els punts (x_i, f_i) coneguts en ordre creixent de les abscises:");
        for(i = 0; i < n; i++)
        {
                scanf("%le", &x[i]);
                scanf("%le", &y[i]);
        }

        printf("Escribint resultats al fitxer polinomis.dat...\n");

        //cada li
        for(i = 0; i <= n; i++)
        {
                //avaluem 100 punts
                for(k = x[0]; k <= x[n-1]; k+=(double)(x[n-1]-x[0])/100.0)
                {
                            //li en cada punt
                      fprintf(f, "%le %le\n", k, Li(i,n,x,k));
                }
                fprintf(f, "\n\n");
        }

        //fem el polinomi en 100 punts
        for(k = x[0]; k <= x[n-1]; k+= ((double)x[n-1]-x[0])/100.0)
        {
                fprintf(f, "%le %le\n", k, p(n, x, y, k));
        }

        printf("Done.\n");
        free(x);
        free(y);
        fclose(f);
        return 0;
}

double Li(int i, int n, double *x, double X)
{
        int j;
        double prod = 1;
        for(j = 0; j < n; j++)
        {
                if(i != j)
                {
                        prod *= (X-x[j])/(x[i]-x[j]);
                }
        }
        return prod;
}
double p(int n, double *x, double *y, double X)
{
        int i;
        double sum = 0;
        for(i = 0; i < n; i++)
        {
                sum += y[i]*Li(i, n, x, X);
        }
        return sum;
}

double p_li(int n, double *y, double *li)
{
        int i;
        double sum = 0;
        for(i = 0; i < n; i++)
        {
                sum += y[i]*li[i];
        }
        return sum;
}
