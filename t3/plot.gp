set terminal svg size 400,300 enhanced fname 'arial'  fsize 10 butt solid
set output 'out.svg'

stats 'dat3.txt' using 0 nooutput
plot for [i=0:(STATS_blocks - 1)] 'dat3.txt' using 1:2 index i with lines notitle
