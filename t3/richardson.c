#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double D0(double h, double a);
double f(double x);

int main(void)
{
    double x, h, prec;
    int i, j, max = 6;
    int p, q;
    
    double **M = (double **)malloc(max*sizeof(double*));
    for(i = 0; i < max; i++)
    {
        M[i] = (double *)malloc((i+1)*sizeof(double));
    }

    printf("Extrapolació de Richardson \n\nIntroduiu x (punt on es vol evaluar la derivada), h, i la precisió:\n");
    scanf("%le", &x);    
    scanf("%le", &h);
    scanf("%le", &prec);

    q = 2;

    for(i = 0; i < max; i++)
    {
        M[i][0] = D0(h/pow(q, i), x);
        printf("%le ", M[i][0]);
        for(j = 1; j <= i; j++)
        {
            p = 2*j;
            M[i][j] = (pow(q, p)*M[i][j-1]-M[i-1][j-1])/(pow(q, p)-1);
            printf("%le ", M[i][j]);
        }

        if(i != 0)
        {
            printf("   Dif: %le", M[i][j-1]-M[i][j-2]);
        }
        if(M[i][j-1]-M[i][j-2] <= prec)
        {
            printf("Ja estem!!!");
        }
        printf("\n");
    }
    return 0;
}

double D0(double h, double a)
{
    return (f(a+h)-f(a-h))/(2*h);
}

double f(double x)
{
    return exp(x);
}
