#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double f(double x);

int main(void)
{   
    double x, h, prec;
    int i, j, k, max = 6;
    int p, q;
    double a, b, sum;
    double *tmp;
    double *rp = (double *)malloc(max*sizeof(double)); /* Romberg previous */
    double *rc = (double *)malloc(max*sizeof(double)); /* Romberg current */
    printf("Integració per Ronberg \n\nIntroduiu els extrems de l'interval a integrar, i la precisió:\n");
    scanf("%le", &a);    
    scanf("%le", &b);
    scanf("%le", &prec);

    h = b-a;
    rp[0] = (f(a)+f(b))*h/2;
    printf("%le \n", rp[0]);

    for(i = 1; i < max; i++)
    {  
        h = h/2;
        sum = 0;
        for(k = 1; k <=  pow(2, i-1); k++)
        {
            sum += f(a+(2*k-1)*h);
        }
        rc[0] = h*sum + rp[0]/2; 
        printf("%le ", rc[0]);

        for(j = 1; j <= i; j++)
        {
            rc[j] = (pow(4,j)*rc[j-1] - rp[j-1])/(pow(4,j)-1);
            printf("%le ", rc[j]);
        }
        printf("\n");
	
        if(i != 0)
        {
            printf("   Dif: %le", rc[i]-rc[i-1]);
			if(rc[i]-rc[i-1]<= prec)
			{
				printf("Ja estem!!!");
			}
		}
       
        tmp = rp;
        rp = rc;
        rc = tmp;
    }
   return 0;
}

double f(double x)
{
    return sin(x);
}
