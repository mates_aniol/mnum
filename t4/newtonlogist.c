/* newtonlogist.c
punts peri�dics de l'aplicaci� log�stica p(x)=4x(1-x)
per�odes: 1, 2, 3, 4, 5, ..., nf (no gaire gran)
tots s�n a [0,1]
m�tode: per a cada n separadament, escombrada + Newton-Raphson
*/
#include <stdio.h>
#include <math.h>

/* es passa tamb� el valor de n a les funcions aval i newton */
void aval(int n, double x, double *ff, double *dd);
int newton(int n, double *x, double prec1, double prec2, int imax);

int main(void) {
   int n, nfi, i, imax, index;
   double x, x1, x2, h, f1, f2, d1, d2, prec1, prec2;

   /* els par�metres seg�ents es podrien llegir */
   imax = 10;
   prec1 = 1e-14;
   prec2 = 1e-14;
   h = 1e-4; /* seria millor usar un pas h diferent per a cada n */

   printf("nfi = ?\n");
   scanf("%d", &nfi);
   if (nfi<1) return 0;
   
   for (n=1; n<=nfi; n++) {
      printf("cas n = %d, ", n);
      printf("arrels de p**%d (x) = x : \n", n);
      i = 1;
      printf("i=%3d x_i=%+.16le \n", i, 0.);
      
      x1 = 1e-8;
      aval(n, x1, &f1, &d1);
      while (i<pow(2,n) && x1<1) {
         x2 = x1+h;
         aval(n, x2, &f2, &d2);
         while (x1<1+h &&f1*f2>0) {
            x1 = x2;
            f1 = f2;
            x2 = x1+h;
            aval(n, x2, &f2, &d2);
         }
         if (x1 >= 1+h) break;
         x = 0.5*(x1+x2);
         index = newton(n, &x, prec1, prec2, imax);
         i = i+1;
         if (index == 0) 
            printf("i=%3d x_i=%+.16le \n", i, x);
         else 
            printf("i=%3d, no converg�ncia \n", n);
         x1 = x2;
         f1 = f2;
      }
      if (i < pow(2,n) ) 
         printf("no s'han trobat totes les arrels, s'aconsella disminuir h\n");
      printf("\n");
   }

   return 0;
}

/* m�tode de NR: retorna 0 si es considera que hi ha converg�ncia */
int newton(int n, double *x, double prec1, double prec2, int imax)
{
   int k, index;
   double xi, f, d;
   
   aval(n, *x, &f, &d);
   k = 0;

   /* escriptura optativa, per a tenir control de qu� passa */
   /*printf("      k=%2d x=%+.16le f=%+le d=%+le \n", k, *x, f, d); 
   */
   xi = 2*prec1;
   while (k <= imax && fabs(xi) > prec1 && fabs(f) > prec2){
      xi = f/d;
      *x = *x-xi;
      aval(n, *x, &f, &d);
      k = k+1;
      
      /* escriptura optativa, per a tenir control de qu� passa */
      /*printf("      k=%2d x=%+.16le f=%+le d=%+le xi=%+le\n", k, *x, f, d, xi); 
      */
   }

   if (k > imax)  index = 1;
   else           index = 0;

   return index;
}

/* avaluaci� de   ff = f(x) = (p**n)(x)-x   i de   dd = f'(x)  */
void aval(int n, double x, double *ff, double *dd) {
   int i;
   double f, d;

   f = x;
   d = 1;
   for (i=1; i<=n; i++) {
      d = 4*(d*(1-f)-f*d);
      f = 4*f*(1-f);
   }

   (*ff) = f-x;
   (*dd) = d-1;
   
   return;
}
