#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double * coefs;

int secant(double *x0, double *x1, int n, double prec1, double prec2, int max);
double eval(double *coefs, int n,  double x );

int main(void)
{
    int n, i, imax, last_index;
    double *roots, prec1, prec2, loc_sum, cota, prev, act, j;
    double pas = 1e-3;
    /* Emmagatzemem el polinomi al que volem buscar els 0's */
    imax = 10;
    prec1 = 1e-14;
    prec2 = 1e-14;
 
    printf("Introduïu el grau del polinomi: ");
    scanf("%d", &n);
    coefs = (double *)malloc((n+1)*sizeof(double));
    roots = (double *)malloc(2*(n+1)*sizeof(double));

    printf("\nIntroduïu els coeficients del polinomi (a_0 ... a_n): ");
    for(i = 0; i <= n; i++)
    {
        scanf("%le", &coefs[i]);
    }
    
    /* Localització */
    for(i = 0; i <= n; i++)
    {
        loc_sum += fabs(coefs[i]/coefs[n]);
    }
    cota = fmax(1.0, loc_sum);
    printf("\nCota: %le\n", cota);

    /* Separació */
    prev = -cota;
    last_index = 0;
    for(j = -cota + pas; j <= cota; j += pas)
    {
        if(eval(coefs, n, prev)*eval(coefs, n, j) < 0)
        {
            printf("Arrel entre %le i %le\n", prev, j);
            roots[last_index] = prev;
            roots[last_index + 1] = j;
            last_index += 2;
        }
        prev = j;

    }

    /* Trobem arrels */

    for(i = 0; i < last_index; i += 2)
    {
        if(!secant(&roots[i], &roots[i+1], n,  prec1, prec2, imax))
        {
            printf("Arrel de p(x): %le\n", roots[i+1]); 
        }
   
    }

}


int secant(double *x0, double *x1, int n,  double prec1, double prec2, int max)
{
    int iterations = 0;
    double tmp;
    double f = eval(coefs, n, *x1);
    while(iterations < max && fabs(*x1 - *x0) >= prec1 && fabs(eval(coefs, n, *x1)) >= prec2)
    {
        tmp = *x1;
        *x1 = *x1 - eval(coefs, n, *x1)*((*x1 - *x0)/(eval(coefs, n, *x1) - eval(coefs, n, *x0)));
        *x0 = tmp;
        iterations += 1;
    }
    if(iterations < max)    return 0;
    else                    return 1;
    
}

double eval(double *coefs, int n,  double x)
{
    int i;
    double total = coefs[0];
    for(i = 1; i <= n; i++)
    {
        total += coefs[i]*pow(x,i);
    }
    return total;
}
